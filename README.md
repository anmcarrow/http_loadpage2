This is just a simple shellcode plugin
for [Munin](http://munin-monitoring.org) monitoring system.

This plugin will check load time of website. 
I was fork this plugin from default distribution plugin 
`http_loadtime`, because this original plugin was not working
in my Ubuntu 12.04.

Have fun with it!
